from datetime import datetime
from typing import Any, Dict, Optional
import copy
import requests
from pendulum import parse
from pendulum import parse
from memoization import cached
from tap_frontapp.client import OAuth2Authenticator, FrontAppStream
from singer_sdk.exceptions import FatalAPIError, RetriableAPIError


class FrontAppSearchStream(FrontAppStream):

    records_jsonpath = "$._results[*]"
    next_page_token_jsonpath = "$._pagination.next"
    url_base = "https://api2.frontapp.com"

    def get_url(self, context: Optional[dict]) -> str:
        """Get stream entity URL."""
        # start_date must be > 1e9
        if self.config.get("get_all_conversations"):
            start_date = int(1e9)
        else:
            start_date = self.get_starting_time(context)
            start_date = int(start_date.timestamp())
            start_date = int(1e9) if start_date<1e9 else start_date
        url = "".join([self.url_base, self.path, f"after:{start_date}"])
        vals = copy.copy(dict(self.config))
        vals.update(context or {})
        for k, v in vals.items():
            search_text = "".join(["{", k, "}"])
            if search_text in url:
                url = url.replace(search_text, self._url_encode(v))
        return url

    @property
    def authenticator(self) -> OAuth2Authenticator:
        url = f"{self.url_base}/oauth/token"
        return OAuth2Authenticator(self, self._tap._config,url)

    def get_next_page_token(
        self, response: requests.Response, previous_token: Optional[Any]
    ) -> Optional[Any]:

        resp = response.json()["_pagination"]["next"]
        if resp:
            next_page_token = [x for x in resp.split("&") if "page_token" in x][0]
            return next_page_token.split("=")[1]
        return None

    def get_url_params(
        self, context: Optional[dict], next_page_token: Optional[Any]
    ) -> Dict[str, Any]:
        params: dict = {}
        if next_page_token:
            params["page_token"] = next_page_token
        return params
