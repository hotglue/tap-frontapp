from datetime import datetime
from typing import Any, Dict, Optional
import time
import requests
import logging
from pendulum import parse
from memoization import cached
from tap_frontapp.auth import OAuth2Authenticator
from singer_sdk.streams import RESTStream
from singer_sdk.exceptions import FatalAPIError, RetriableAPIError


class FrontAppStream(RESTStream):

    records_jsonpath = "$._results[*]"
    next_page_token_jsonpath = "$._pagination.next"
    url_base = "https://api2.frontapp.com"

    @cached
    def get_starting_time(self, context):
        start_date = parse(self.config.get("start_date"))
        rep_key = self.get_starting_timestamp(context)
        return rep_key or start_date

    @property
    def authenticator(self) -> OAuth2Authenticator:
        url = f"{self.url_base}/oauth/token"
        return OAuth2Authenticator(self, self._tap._config,url)

    def get_next_page_token(
        self, response: requests.Response, previous_token: Optional[Any]
    ) -> Optional[Any]:

        resp = response.json()["_pagination"]["next"]
        if resp:
            next_page_token = [x for x in resp.split("&") if "page_token" in x][0]
            return next_page_token.split("=")[1]
        return None

    def get_url_params(
        self, context: Optional[dict], next_page_token: Optional[Any]
    ) -> Dict[str, Any]:

        params: dict = {}

        if next_page_token:
            params["page_token"] = next_page_token
        if self.replication_key == "updated_at":
            params["sort_by"] = self.replication_key
            params["sort_order"] = "desc"
        rep_key_value = self.get_starting_time(context)
        if rep_key_value:
            time_stamp = rep_key_value.timestamp()
            #API allows timestamp in seconds with up to 3 decimal places only.
            time_stamp = "{:.3f}".format(time_stamp)
            params["q[updated_after]"] = time_stamp
        return params

    def get_datetime_keys(self, schema):
        output = {}
        for key, value in schema["properties"].items():
            if value.get("format") == "date-time":
                output[key] = "date-time"
            elif "array" in value.get("type") and "items" in value and "object" in value["items"]["type"] and value["items"].get("properties"):
                dt_items = self.get_datetime_keys(value["items"])
                if dt_items:
                    output[key] = [dt_items]
            elif "object" in value.get("type") and value.get("properties"):
                dt_items = self.get_datetime_keys(value)
                if dt_items:
                    output[key] = dt_items
        return output

    @property
    @cached
    def datetime_fields(self):
        return self.get_datetime_keys(self.schema)

    def to_datetime(self, datetime_field, row):
        for field in datetime_field:
            if isinstance(row.get(field), dict):
                row[field] = self.to_datetime(datetime_field[field], row[field])
            elif isinstance(row.get(field), list):
                row[field] = [self.to_datetime(datetime_field[field][0], f) for f in row[field]]
            elif row.get(field):
                row[field] = datetime.utcfromtimestamp(row[field]).replace(tzinfo=None).isoformat()
        return row

    def post_process(self, row: dict, context: Optional[dict]) -> dict:
        """As needed, append or transform raw data to match expected structure."""
        row = self.to_datetime(self.datetime_fields, row)
        if self.replication_key:
            start_date = self.get_starting_time(context)
            row_start = parse(row.get(self.replication_key))
            if start_date < row_start:
                return row
        else:
            return row

    def validate_response(self, response: requests.Response) -> None:
        
        if response.status_code == 429:
            wait_time = response.headers.get("Retry-After", 30)
            logging.info(f"Waiting {wait_time} seconds...")
            time.sleep(int(wait_time))
            msg = self.response_error_message(response)
            raise RetriableAPIError(msg, response)
        elif (
            500 <= response.status_code < 600
        ):
            msg = self.response_error_message(response)
            raise RetriableAPIError(msg, response)
        elif 400 <= response.status_code < 500:
            msg = self.response_error_message(response)
            raise FatalAPIError(msg)