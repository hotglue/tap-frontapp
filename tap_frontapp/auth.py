"""hubspot Authentication."""

import json
from datetime import datetime
from typing import Any, Dict, Optional

import requests
from requests.auth import HTTPBasicAuth
from singer_sdk.authenticators import APIAuthenticatorBase
from singer_sdk.streams import Stream as RESTStreamBase


class OAuth2Authenticator(APIAuthenticatorBase):

    def __init__(
        self,
        stream: RESTStreamBase,
        config_file: Optional[str] = None,
        auth_endpoint: Optional[str] = None,
    ) -> None:
        super().__init__(stream=stream)
        self._auth_endpoint = auth_endpoint
        self._config_file = config_file
        self._tap = stream._tap
        self.client_id = config_file.get("client_id")
        self.client_secret = config_file.get("client_secret")

    @property
    def auth_headers(self) -> dict:

        if not self.is_token_valid():
            self.update_access_token()
        result = super().auth_headers
        result["Authorization"] = f"Bearer {self._tap._config.get('access_token')}"
        return result

    @property
    def auth_endpoint(self) -> str:

        if not self._auth_endpoint:
            raise ValueError("Authorization endpoint not set.")
        return self._auth_endpoint

    @property
    def oauth_request_body(self) -> dict:
        return {
            "refresh_token": str(self._tap._config["refresh_token"]),
            "grant_type": "refresh_token",
        }

    def is_token_valid(self) -> bool:
        access_token = self._tap._config.get("access_token")
        now = round(datetime.utcnow().timestamp())
        expires_in = self._tap._config.get("last_update")

        return not bool(
            (not access_token) or (not expires_in) or ((expires_in + 600 - now) < 60)
        )

    @property
    def oauth_request_payload(self) -> dict:

        return self.oauth_request_body

    # Authentication and refresh
    def update_access_token(self) -> None:

        request_time = round(datetime.utcnow().timestamp())
        auth_request_payload = self.oauth_request_payload
        token_response = requests.post(
            'https://app.frontapp.com/oauth/token',
            data = auth_request_payload,
            auth=HTTPBasicAuth(self.client_id,self.client_secret)
            )
        try:
            token_response.raise_for_status()
            self.logger.info("OAuth authorization attempt was successful.")
        except Exception as ex:
            raise RuntimeError(
                f"Failed OAuth login, response was '{token_response.json()}'. {ex}"
            )
        token_json = token_response.json()
        self.access_token = token_json["access_token"]

        self._tap._config["access_token"] = token_json["access_token"]
        self._tap._config["last_update"] = request_time
        with open(self._tap.config_file, "w") as outfile:
            json.dump(self._tap._config, outfile, indent=4)
