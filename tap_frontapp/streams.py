from singer_sdk import typing as th

from tap_frontapp.client import FrontAppStream
from tap_frontapp.client_search import FrontAppSearchStream
from typing import Optional
from pendulum import parse
from memoization import cached


class ContactsStream(FrontAppStream):
    """Define custom stream."""

    name = "contacts"
    path = "/contacts"
    primary_keys = ["id"]
    replication_key = "updated_at"
    schema = th.PropertiesList(
        th.Property(
            "_links",
            th.ObjectType(
                th.Property("self", th.StringType),
                th.Property(
                    "related",
                    th.ObjectType(
                        th.Property("notes", th.StringType),
                        th.Property("conversations", th.StringType),
                        th.Property("owner", th.StringType),
                    ),
                ),
            ),
        ),
        th.Property("id", th.StringType),
        th.Property("name", th.StringType),
        th.Property("description", th.StringType),
        th.Property("avatar_url", th.StringType),
        th.Property("links", th.CustomType({"type": ["array", "string"]})),
        th.Property(
            "handles",
            th.ArrayType(
                th.ObjectType(
                    th.Property("source", th.StringType),
                    th.Property("handle", th.StringType),
                )
            ),
        ),
        th.Property(
            "groups",
            th.ArrayType(
                th.ObjectType(
                    th.Property(
                        "_links",
                        th.ObjectType(
                            th.Property("self", th.StringType),
                            th.Property(
                                "related",
                                th.ObjectType(
                                    th.Property("contacts", th.StringType),
                                    th.Property("owner", th.StringType),
                                ),
                            ),
                        ),
                    ),
                    th.Property("id", th.StringType),
                    th.Property("name", th.StringType),
                    th.Property("is_private", th.BooleanType),
                )
            ),
        ),
        th.Property("updated_at", th.DateTimeType),
        th.Property("custom_fields", th.CustomType({"type": ["object", "string"]})),
        th.Property("is_private", th.BooleanType),
        th.Property("account", th.CustomType({"type": ["object","array", "string","number"]})),
    ).to_dict()


class ConversationsStream(FrontAppSearchStream):

    name = "conversations"
    path = "/conversations/search/"
    primary_keys = ["id"]
    replication_key = "created_at"

    schema = th.PropertiesList(
        th.Property("_links",th.CustomType({"type":["object","string"]})),
        th.Property("id",th.StringType),
        th.Property("subject",th.StringType),
        th.Property("status",th.StringType),
        th.Property("assignee",th.CustomType({"type":["object","string","boolean"]})),
        th.Property("recipient",th.CustomType({"type":["object","string"]})),
        th.Property("tags",
            th.ArrayType(
                th.ObjectType(
                    th.Property("id",th.StringType),
                    th.Property("name",th.StringType),
                    th.Property("highliht",th.StringType),
                    th.Property("is_private",th.BooleanType),
                    th.Property("is_visible_in_conversation_lists",th.BooleanType),
                    th.Property("created_at",th.DateTimeType),
                    th.Property("updated_at",th.DateTimeType),
                )
            )
        ),
        th.Property("links",th.CustomType({"type":["array","object","string"]})),
        th.Property("last_message",
            th.ObjectType(
                th.Property("_links",th.CustomType({"type":["object","string"]})),
                th.Property("id",th.StringType),
                th.Property("type",th.StringType),
                th.Property("is_inbound",th.BooleanType),
                th.Property("created_at",th.DateTimeType),
                th.Property("blurb",th.StringType),
                th.Property("body",th.StringType),
                th.Property("text",th.StringType),
                th.Property("error_type",th.StringType),
                th.Property("version",th.StringType),
                th.Property("subject",th.StringType),
                th.Property("draft_mode",th.StringType),
                th.Property("metadata",th.CustomType({"type":["object","string","boolean"]})),
                th.Property("author",th.CustomType({"type":["object","string","boolean"]})),
                th.Property("recipients",
                    th.ArrayType(
                        th.ObjectType(
                            th.Property("_links",th.CustomType({"type":["object","string"]})),
                            th.Property("name",th.StringType),
                            th.Property("handle",th.StringType),
                            th.Property("role",th.StringType),
                        )
                    )
                ),
                th.Property("attachments",th.CustomType({"type":["array","object","string"]})),
                th.Property("signature",th.CustomType({"type":["object","string","boolean"]})),
                th.Property("is_draft",th.BooleanType),
            )
        ),
        th.Property("created_at",th.DateTimeType),
        th.Property("is_private",th.BooleanType),
        th.Property("scheduled_reminders",
            th.ArrayType(
                    th.ObjectType(
                        th.Property("_links",th.CustomType({"type":["object","string"]})),
                        th.Property("created_at",th.DateTimeType),
                        th.Property("scheduled_at",th.DateTimeType),
                        th.Property("updated_at",th.DateTimeType),
                    )
            )
        ),
        th.Property("metadata",th.CustomType({"type":["object","string","boolean"]})),
    ).to_dict()

    def get_child_context(self, record: dict, context: Optional[dict]) -> dict:
        """Return a context dictionary for child streams."""
        return {"conversation_id": record["id"]}


class ConversationMessagesStream(FrontAppStream):

    name = "conversation_messages"
    path = "/conversations/{conversation_id}/messages"
    primary_keys = ["id"]
    parent_stream_type = ConversationsStream
    replication_key = "created_at"

    schema = th.PropertiesList(
        th.Property("_links",th.CustomType({"type":["object","string"]})),
        th.Property("id",th.StringType),
        th.Property("type",th.StringType),
        th.Property("is_inbound",th.BooleanType),
        th.Property("created_at",th.DateTimeType),
        th.Property("blurb",th.StringType),
        th.Property("body",th.StringType),
        th.Property("text",th.StringType),
        th.Property("error_type",th.StringType),
        th.Property("version",th.StringType),
        th.Property("subject",th.StringType),
        th.Property("draft_mode",th.StringType),
        th.Property("metadata",th.CustomType({"type":["object","string","boolean"]})),
        th.Property("author",th.CustomType({"type":["object","string","boolean"]})),
        th.Property("recipients",
            th.ArrayType(
                th.ObjectType(
                    th.Property("_links",th.CustomType({"type":["object","string"]})),
                    th.Property("name",th.StringType),
                    th.Property("handle",th.StringType),
                    th.Property("role",th.StringType),
                )
            )
        ),
        th.Property("attachments",th.CustomType({"type":["array","object","string"]})),
        th.Property("signature",th.CustomType({"type":["object","string","boolean"]})),
        th.Property("is_draft",th.BooleanType),
        th.Property("conversation_id",th.StringType),
    ).to_dict()

    @cached
    def get_starting_time(self, context):
        start_date = parse(self.config.get("start_date"))
        rep_key = self.get_starting_timestamp(context)
        conv_state = self._tap.state.get("bookmarks", {}).get("conversations", {}).get("starting_replication_value")
        if conv_state:
            conv_state = parse(conv_state)
            if conv_state > start_date:
                start_date = conv_state
        if rep_key and start_date:
            if start_date > rep_key:
                rep_key = start_date
        return rep_key or start_date


class InboxesStream(FrontAppStream):

    name = "inboxes"
    path = "/conversations/{conversation_id}/inboxes"
    parent_stream_type = ConversationsStream
    primary_keys = ["id"]

    schema = th.PropertiesList(
        th.Property("_links",th.CustomType({"type":["object","string"]})),
        th.Property("id",th.StringType),
        th.Property("name",th.StringType),
        th.Property("is_private",th.BooleanType),
        th.Property("is_public",th.BooleanType),
        th.Property("address",th.StringType),
        th.Property("send_as",th.StringType),
        th.Property("type",th.StringType),
        th.Property("custom_fields",th.CustomType({"type":["object","string"]})),
        th.Property("conversation_id",th.StringType),
    ).to_dict()

