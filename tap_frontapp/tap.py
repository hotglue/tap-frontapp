from singer_sdk import Stream, Tap
from singer_sdk import typing as th
from typing import Any, Callable, Dict, List, Optional, Tuple, Type, Union, cast

from tap_frontapp.streams import ContactsStream, ConversationMessagesStream,ConversationsStream,InboxesStream

STREAM_TYPES = [ContactsStream,
                ConversationMessagesStream,
                ConversationsStream,
                InboxesStream]


class TapFrontApp(Tap):
    name = "tap-frontapp"

    def __init__(
        self,
        config=None,
        catalog=None,
        state=None,
        parse_env_config=False,
        validate_config=True,
    ) -> None:
        self.config_file = config[0]
        super().__init__(config, catalog, state, parse_env_config, validate_config)

    config_jsonschema = th.PropertiesList(
        th.Property(
            "access_token",
            th.StringType,
            required=False,
            description="The token to authenticate against the API service",
        ),
        th.Property(
            "refresh_token",
            th.StringType,
            required=True
        ),
        th.Property(
            "client_id",
            th.StringType,
            required=True
        ),
        th.Property(
            "client_secret",
            th.StringType,
            required=True
        ),
        th.Property(
            "start_date",
            th.DateTimeType,
            description="The earliest record date to sync",
        ),
    ).to_dict()

    def discover_streams(self) -> List[Stream]:
        """Return a list of discovered streams."""
        return [stream_class(tap=self) for stream_class in STREAM_TYPES]


if __name__ == "__main__":
    TapFrontApp.cli()
